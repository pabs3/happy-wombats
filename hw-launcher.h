/* hw-launcher.h */

#ifndef _HW_LAUNCHER_H
#define _HW_LAUNCHER_H

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define HW_TYPE_LAUNCHER hw_launcher_get_type()

#define HW_LAUNCHER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  HW_TYPE_LAUNCHER, HwLauncher))

#define HW_LAUNCHER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  HW_TYPE_LAUNCHER, HwLauncherClass))

#define HW_IS_LAUNCHER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  HW_TYPE_LAUNCHER))

#define HW_IS_LAUNCHER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  HW_TYPE_LAUNCHER))

#define HW_LAUNCHER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  HW_TYPE_LAUNCHER, HwLauncherClass))

typedef struct _HwLauncher HwLauncher;
typedef struct _HwLauncherClass HwLauncherClass;
typedef struct _HwLauncherPrivate HwLauncherPrivate;

struct _HwLauncher
{
  ClutterActor parent;

  HwLauncherPrivate *priv;
};

struct _HwLauncherClass
{
  ClutterActorClass parent_class;

  /* signals */
  void (* launch) (gdouble angle, gdouble power);
};

GType hw_launcher_get_type (void) G_GNUC_CONST;

ClutterActor *hw_launcher_new (void);

void  hw_launcher_set_ammo (HwLauncher *launcher, guint rounds);
guint hw_launcher_get_ammo (HwLauncher *launcher);

G_END_DECLS

#endif /* _HW_LAUNCHER_H */
