/* hw-circle.c */

#include "hw-circle.h"

G_DEFINE_TYPE (HwCircle, hw_circle, CLUTTER_TYPE_ACTOR)

#define CIRCLE_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), HW_TYPE_CIRCLE, HwCirclePrivate))

enum
{
  PROP_0,

  PROP_COLOR
};

struct _HwCirclePrivate
{
  ClutterColor color;
};

static const ClutterColor white = { 0xff, 0xff, 0xff, 0xff };

static void
hw_circle_get_property (GObject    *object,
                        guint       property_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  HwCirclePrivate *priv = HW_CIRCLE (object)->priv;

  switch (property_id)
    {
    case PROP_COLOR:
      clutter_value_set_color (value, &priv->color);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
hw_circle_set_property (GObject      *object,
                        guint         property_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  HwCircle *self = HW_CIRCLE (object);

  switch (property_id)
    {
    case PROP_COLOR:
      hw_circle_set_color (self, clutter_value_get_color (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
hw_circle_dispose (GObject *object)
{
  G_OBJECT_CLASS (hw_circle_parent_class)->dispose (object);
}

static void
hw_circle_finalize (GObject *object)
{
  G_OBJECT_CLASS (hw_circle_parent_class)->finalize (object);
}

static void
hw_circle_draw_circle (gfloat radius, const ClutterColor *color, guint8 alpha)
{
  cogl_set_source_color4ub (color->red,
                            color->green,
                            color->blue,
                            (guint8)(color->alpha * (alpha/255.f)));
  cogl_path_arc (radius, radius, radius, radius, 0, 360);
  cogl_path_fill ();
}

static void
hw_circle_paint (ClutterActor *actor)
{
  HwCirclePrivate *priv = HW_CIRCLE (actor)->priv;
  hw_circle_draw_circle (MIN (clutter_actor_get_width (actor),
                              clutter_actor_get_height (actor)) / 2.f,
                         &priv->color,
                         clutter_actor_get_paint_opacity (actor));
}

static void
hw_circle_pick (ClutterActor *actor, const ClutterColor *color)
{
  hw_circle_draw_circle (MIN (clutter_actor_get_width (actor),
                              clutter_actor_get_height (actor)) / 2.f,
                         color,
                         255);
}

static void
hw_circle_class_init (HwCircleClass *klass)
{
  GParamSpec *pspec;

  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);

  g_type_class_add_private (klass, sizeof (HwCirclePrivate));

  object_class->get_property = hw_circle_get_property;
  object_class->set_property = hw_circle_set_property;
  object_class->dispose = hw_circle_dispose;
  object_class->finalize = hw_circle_finalize;

  actor_class->paint = hw_circle_paint;
  actor_class->pick = hw_circle_pick;

  pspec = clutter_param_spec_color ("color",
                                    "Color",
                                    "Circle color",
                                    &white,
                                    G_PARAM_STATIC_STRINGS |
                                    G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_COLOR, pspec);
}

static void
hw_circle_init (HwCircle *self)
{
  HwCirclePrivate *priv = self->priv = CIRCLE_PRIVATE (self);

  priv->color = white;
}

ClutterActor *
hw_circle_new (void)
{
  return g_object_new (HW_TYPE_CIRCLE, NULL);
}

ClutterActor *
hw_circle_new_with_color (const ClutterColor *color)
{
  return g_object_new (HW_TYPE_CIRCLE, "color", color, NULL);
}

void
hw_circle_get_color (HwCircle *circle, ClutterColor *color)
{
  g_return_if_fail (HW_IS_CIRCLE (circle));
  g_return_if_fail (color);

  *color = circle->priv->color;
}

void
hw_circle_set_color (HwCircle *circle, const ClutterColor *color)
{
  HwCirclePrivate *priv;

  g_return_if_fail (HW_IS_CIRCLE (circle));
  g_return_if_fail (color);

  priv = circle->priv;

  if (color->red != priv->color.red ||
      color->green != priv->color.green ||
      color->blue != priv->color.blue ||
      color->alpha != priv->color.alpha)
    {
      priv->color = *color;
      g_object_notify (G_OBJECT (circle), "color");
    }
}
