/* hw-scene.c */

#include "hw-scene.h"
#include "hw-marshal.h"
#include "hw-utils.h"
#include <mx/mx.h>

static void mx_scrollable_interface_init (MxScrollableIface *iface);

G_DEFINE_TYPE_WITH_CODE (HwScene, hw_scene, CLUTTER_TYPE_BOX2D,
                         G_IMPLEMENT_INTERFACE (MX_TYPE_SCROLLABLE,
                                                mx_scrollable_interface_init))

#define SCENE_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), HW_TYPE_SCENE, HwScenePrivate))

#define GROUND_OFFSET 16

enum
{
  PROP_0,

  PROP_HADJUST,
  PROP_VADJUST,
  PROP_ENABLE_TRACKING
};

enum
{
  COMPLETE,
  EXPLODED,

  LAST_SIGNAL
};

struct _HwScenePrivate
{
  guint            enable_tracking : 1;
  guint            complete        : 1;

  ClutterActor    *sun;
  ClutterActor    *sky;
  ClutterActor    *bgs[3];
  ClutterActor    *ground;
  ClutterTimeline *daynight;

  MxAdjustment    *hadjust;
  MxAdjustment    *vadjust;

  ClutterActorBox  focus_area;
};

static guint signals[LAST_SIGNAL] = { 0, };


static void
hw_scene_get_adjustments (MxScrollable  *scrollable,
                          MxAdjustment **hadjust,
                          MxAdjustment **vadjust)
{
  HwScenePrivate *priv = HW_SCENE (scrollable)->priv;

  if (hadjust)
    *hadjust = priv->hadjust;
  if (vadjust)
    *vadjust = priv->vadjust;
}

static void
hw_scene_set_adjustments (MxScrollable *scrollable,
                          MxAdjustment *hadjust,
                          MxAdjustment *vadjust)
{
  HwScenePrivate *priv = HW_SCENE (scrollable)->priv;

  if (hadjust && (hadjust != priv->hadjust))
    {
      g_signal_handlers_disconnect_by_func (priv->hadjust,
                                            clutter_actor_queue_relayout,
                                            scrollable);
      g_object_unref (priv->hadjust);
      priv->hadjust = g_object_ref (hadjust);
      g_signal_connect_swapped (priv->hadjust, "notify::value",
                                G_CALLBACK (clutter_actor_queue_relayout),
                                scrollable);
    }

  if (vadjust && (vadjust != priv->vadjust))
    {
      g_signal_handlers_disconnect_by_func (priv->vadjust,
                                            clutter_actor_queue_relayout,
                                            scrollable);
      g_object_unref (priv->vadjust);
      priv->vadjust = g_object_ref (vadjust);
      g_signal_connect_swapped (priv->vadjust, "notify::value",
                                G_CALLBACK (clutter_actor_queue_relayout),
                                scrollable);
    }
}

static void
mx_scrollable_interface_init (MxScrollableIface *iface)
{
  iface->set_adjustments = hw_scene_set_adjustments;
  iface->get_adjustments = hw_scene_get_adjustments;
}

static void
hw_scene_get_property (GObject    *object,
                       guint       property_id,
                       GValue     *value,
                       GParamSpec *pspec)
{
  HwScenePrivate *priv = HW_SCENE (object)->priv;

  switch (property_id)
    {
    case PROP_HADJUST:
      g_value_set_object (value, priv->hadjust);
      break;

    case PROP_VADJUST:
      g_value_set_object (value, priv->vadjust);
      break;

    case PROP_ENABLE_TRACKING:
      g_value_set_boolean (value, priv->enable_tracking);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
hw_scene_set_property (GObject      *object,
                       guint         property_id,
                       const GValue *value,
                       GParamSpec   *pspec)
{
  switch (property_id)
    {
    case PROP_HADJUST:
      hw_scene_set_adjustments (MX_SCROLLABLE (object),
                                MX_ADJUSTMENT (g_value_get_object (value)),
                                NULL);
      break;

    case PROP_VADJUST:
      hw_scene_set_adjustments (MX_SCROLLABLE (object),
                                NULL,
                                MX_ADJUSTMENT (g_value_get_object (value)));
      break;

    case PROP_ENABLE_TRACKING:
      hw_scene_set_enable_tracking (HW_SCENE (object),
                                    g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
hw_scene_dispose (GObject *object)
{
  gint i;
  HwScenePrivate *priv = HW_SCENE (object)->priv;

  if (priv->daynight)
    {
      g_object_unref (priv->daynight);
      priv->daynight = NULL;
    }

  if (priv->sun)
    {
      clutter_actor_destroy (priv->sun);
      priv->sun = NULL;
    }

  if (priv->sky)
    {
      clutter_actor_destroy (priv->sky);
      priv->sky = NULL;
    }

  for (i = 0; i < G_N_ELEMENTS (priv->bgs); i++)
    {
      if (priv->bgs[i])
        {
          clutter_actor_destroy (priv->bgs[i]);
          priv->bgs[i] = NULL;
        }
    }

  if (priv->hadjust)
    {
      g_object_unref (priv->hadjust);
      priv->hadjust = NULL;
    }

  if (priv->vadjust)
    {
      g_object_unref (priv->vadjust);
      priv->vadjust = NULL;
    }

  G_OBJECT_CLASS (hw_scene_parent_class)->dispose (object);
}

static void
hw_scene_finalize (GObject *object)
{
  G_OBJECT_CLASS (hw_scene_parent_class)->finalize (object);
}

static void
hw_scene_get_preferred_width (ClutterActor *actor,
                              gfloat        for_height,
                              gfloat       *min_width_p,
                              gfloat       *nat_width_p)
{
  CLUTTER_ACTOR_CLASS (hw_scene_parent_class)->
    get_preferred_width (actor, for_height, NULL, nat_width_p);

  if (min_width_p)
    *min_width_p = 0;
}

static void
hw_scene_get_preferred_height (ClutterActor *actor,
                               gfloat        for_width,
                               gfloat       *min_height_p,
                               gfloat       *nat_height_p)
{
  CLUTTER_ACTOR_CLASS (hw_scene_parent_class)->
    get_preferred_height (actor, for_width, NULL, nat_height_p);

  if (min_height_p)
    *min_height_p = 0;
}

static void
hw_scene_allocate (ClutterActor           *actor,
                   const ClutterActorBox  *box,
                   ClutterAllocationFlags  flags)
{
  gint i;
  ClutterActorBox child_box;
  gfloat width, height, x, y;

  HwScenePrivate *priv = HW_SCENE (actor)->priv;

  CLUTTER_ACTOR_CLASS (hw_scene_parent_class)-> allocate (actor, box, flags);

  x = mx_adjustment_get_value (priv->hadjust);
  y = mx_adjustment_get_value (priv->vadjust);

  clutter_actor_get_preferred_size (priv->sun, NULL, NULL, &width, &height);
  child_box.x1 = (clutter_timeline_get_progress (priv->daynight) *
                  (mx_adjustment_get_upper (priv->hadjust) + width)) - width;
  child_box.x2 = child_box.x1 + width;
  child_box.y1 = -y;
  child_box.y2 = child_box.y1 + height;
  clutter_actor_allocate (priv->sun, &child_box, flags);

  child_box.x1 = x;
  child_box.y1 = y;
  child_box.x2 = child_box.x1 + (box->x2 - box->x1);
  child_box.y2 = child_box.y1 + (box->y2 - box->y1);
  clutter_actor_allocate (priv->sky, &child_box, flags);

  child_box.y2 = mx_adjustment_get_upper (priv->vadjust);
  for (i = 0; i < G_N_ELEMENTS (priv->bgs); i++)
    {
      clutter_actor_get_preferred_height (priv->bgs[i], -1, NULL, &height);

      child_box.x1 = (x / (gfloat)(i + 1)) / 2.f;
      child_box.x2 = x + box->x2 - box->x1;
      child_box.y1 = child_box.y2 - height;
      clutter_actor_allocate (priv->bgs[i], &child_box, flags);
    }

  /* Synchronise adjustments */
  g_object_get (G_OBJECT (actor),
                "natural-width", &width,
                "natural-height", &height,
                NULL);
  g_object_set (G_OBJECT (priv->hadjust),
                "lower", 0.0,
                "page-size", box->x2 - box->x1,
                "upper", width,
                "page-increment", (box->x2 - box->x1) / 3.0,
                "step-increment", 1.0,
                NULL);
  g_object_set (G_OBJECT (priv->vadjust),
                "lower", 0.0,
                "page-size", box->y2 - box->y1,
                "upper", height,
                "page-increment", (box->y2 - box->y1) / 3.0,
                "step-increment", 1.0,
                NULL);
}

static void
hw_scene_apply_transform (ClutterActor *actor, CoglMatrix *matrix)
{
  HwScenePrivate *priv = HW_SCENE (actor)->priv;

  CLUTTER_ACTOR_CLASS (hw_scene_parent_class)->
    apply_transform (actor, matrix);

  cogl_matrix_translate (matrix,
                         -mx_adjustment_get_value (priv->hadjust),
                         -mx_adjustment_get_value (priv->vadjust),
                         0);
}

static void
hw_scene_paint (ClutterActor *actor)
{
  gint i;
  gfloat x, y;
  gdouble progress;
  ClutterActorBox box;

  HwScenePrivate *priv = HW_SCENE (actor)->priv;

  clutter_actor_get_allocation_box (actor, &box);
  x = mx_adjustment_get_value (priv->hadjust);
  y = mx_adjustment_get_value (priv->vadjust);
  cogl_clip_push_rectangle (x, y, x + box.x2 - box.x1, y + box.y2 - box.y1);

  clutter_actor_paint (priv->sky);
  clutter_actor_paint (priv->sun);

  for (i = 0; i < G_N_ELEMENTS (priv->bgs); i++)
    clutter_actor_paint (priv->bgs[i]);

  CLUTTER_ACTOR_CLASS (hw_scene_parent_class)->paint (actor);

  /* Draw an alpha-transparent rectangle over the scene to represent the
   * day/night cycle.
   */
  progress = clutter_timeline_get_progress (priv->daynight);
  if (progress < 0.5)
    progress = 1.0;
  else if (progress < 0.75)
    progress = 1.0 - ((progress - 0.5) * 4);
  else
    progress = (progress - 0.75) * 4;

  cogl_set_source_color4f (progress, progress, 0.4 + (0.6 * progress),
                           (1.0 - progress) * 0.4);
  cogl_rectangle (x, y, x + box.x2 - box.x1, y + box.y2 - box.y1);

  cogl_clip_pop ();
}

static void
hw_scene_pick (ClutterActor *actor, const ClutterColor *color)
{
  gfloat x, y;
  ClutterActorBox box;

  HwScenePrivate *priv = HW_SCENE (actor)->priv;

  clutter_actor_get_allocation_box (actor, &box);
  x = mx_adjustment_get_value (priv->hadjust);
  y = mx_adjustment_get_value (priv->vadjust);
  cogl_clip_push_rectangle (x, y, x + box.x2 - box.x1, y + box.y2 - box.y1);

  cogl_set_source_color4ub (color->red,
                            color->green,
                            color->blue,
                            color->alpha);

  cogl_rectangle (x, y, x + box.x2 - box.x1, y + box.y2 - box.y1);

  CLUTTER_ACTOR_CLASS (hw_scene_parent_class)->pick (actor, color);

  cogl_clip_pop ();
}

static void
hw_scene_map (ClutterActor *actor)
{
  gint i;

  HwScenePrivate *priv = HW_SCENE (actor)->priv;

  CLUTTER_ACTOR_CLASS (hw_scene_parent_class)->map (actor);

  clutter_actor_map (priv->sun);
  clutter_actor_map (priv->sky);
  for (i = 0; i < G_N_ELEMENTS (priv->bgs); i++)
    clutter_actor_map (priv->bgs[i]);
}

static void
hw_scene_unmap (ClutterActor *actor)
{
  gint i;

  HwScenePrivate *priv = HW_SCENE (actor)->priv;

  clutter_actor_unmap (priv->sun);
  clutter_actor_unmap (priv->sky);
  for (i = 0; i < G_N_ELEMENTS (priv->bgs); i++)
    clutter_actor_unmap (priv->bgs[i]);

  CLUTTER_ACTOR_CLASS (hw_scene_parent_class)->unmap (actor);
}

static void
hw_scene_constructed (GObject *self)
{
  const ClutterVertex gravity = { 0.f, 9.8f, 0.f };
  HwScenePrivate *priv = HW_SCENE (self)->priv;

  G_OBJECT_CLASS (hw_scene_parent_class)->constructed (self);

  /* Setup the physics properties */
  clutter_box2d_set_gravity (CLUTTER_BOX2D (self), &gravity);
  clutter_box2d_set_scale_factor (CLUTTER_BOX2D (self), 1.f/100.f);

  /* Create the ground plane */
  priv->ground = clutter_texture_new_from_file ("ground.png", NULL);
  clutter_texture_set_repeat (CLUTTER_TEXTURE (priv->ground), TRUE, FALSE);
  clutter_actor_set_name (priv->ground, "Ground");
  clutter_actor_set_width (priv->ground, 1280);
  clutter_actor_set_position (priv->ground, 0,
                              clutter_actor_get_height (CLUTTER_ACTOR (self)) -
                              clutter_actor_get_height (priv->ground) +
                              GROUND_OFFSET);
  clutter_container_add_actor (CLUTTER_CONTAINER (self), priv->ground);
  clutter_container_child_set (CLUTTER_CONTAINER (self), priv->ground,
                               "mode", CLUTTER_BOX2D_STATIC,
                               "density", 7.0f,
                               "friction", 0.4f,
                               "restitution", 0.f,
                               NULL);

  hw_paint_offset (priv->ground, 0, GROUND_OFFSET);
}

typedef struct
{
  ClutterActorBox *box;

  ClutterVertex    fastest_actor;
  ClutterActorBox  actor_box;

  gboolean         has_dingos;
} HwSceneIterateData;

static void
hw_scene_focus_area_cb (ClutterActor *child,
                        gpointer      data)
{
  gfloat width, height;
  ClutterActor *parent;
  ClutterVertex *vertex;
  ClutterBox2DType mode;
  ClutterChildMeta *meta;
  ClutterActorBox child_box;
  HwSceneIterateData *cb_data;

  const gchar *name = clutter_actor_get_name (child);

  if (!name || g_str_equal (name, "Launcher") || g_str_equal (name, "Ground"))
    return;

  cb_data = data;

  if (!cb_data->has_dingos && g_str_equal (name, "Dingo"))
    cb_data->has_dingos = TRUE;

  parent = clutter_actor_get_parent (child);
  clutter_actor_get_preferred_size (parent, NULL, NULL, &width, &height);

  clutter_actor_get_allocation_box (child, &child_box);
  if ((child_box.x1 < child_box.x1 - child_box.x2) ||
      (child_box.x1 > width) ||
      (child_box.y1 > height))
    {
      clutter_actor_destroy (child);
      if (g_str_equal (name, "Dingo"))
        g_signal_emit (parent,
                       signals[EXPLODED],
                       g_quark_from_static_string ("dingo"),
                       child_box.x1 + (child_box.x2 - child_box.x1)/2,
                       child_box.y1 + (child_box.y2 - child_box.y1)/2);
      return;
    }

  meta = clutter_container_get_child_meta (CLUTTER_CONTAINER (parent), child);

  g_object_get (G_OBJECT (meta), "mode", &mode, NULL);
  if (mode == CLUTTER_BOX2D_NONE)
    return;

  g_object_get (G_OBJECT (meta), "linear-velocity", &vertex, NULL);

  if ((vertex->x > 100) || (vertex->y > 100))
    {
      ClutterActorBox *box = cb_data->box;

      if (child_box.x1 < box->x1)
        box->x1 = child_box.x1;
      if (child_box.y1 < box->y1)
        box->y1 = child_box.y1;
      if (child_box.x2 > box->x2)
        box->x2 = child_box.x2;
      if (child_box.y2 > box->y2)
        box->y2 = child_box.y2;
    }

  if ((vertex->x > cb_data->fastest_actor.x) ||
      (vertex->y > cb_data->fastest_actor.y))
    {
      cb_data->fastest_actor = *vertex;
      cb_data->actor_box = child_box;
    }

  clutter_vertex_free (vertex);
}

static void
hw_scene_iterate (ClutterBox2D *box2d)
{
  ClutterActorBox focus_area;

  HwSceneIterateData data = { 0, };
  HwScenePrivate *priv = HW_SCENE (box2d)->priv;

  CLUTTER_BOX2D_CLASS (hw_scene_parent_class)->iterate (box2d);

  /* Iterate through children and expand the focus area with moving children,
   * then try to keep that area visible.
   */
  data.box = &focus_area;
  focus_area = priv->focus_area;
  clutter_container_foreach (CLUTTER_CONTAINER (box2d),
                             hw_scene_focus_area_cb,
                             &data);

  if (priv->enable_tracking)
    {
      gboolean focus_on_movement;
      gdouble value, page_size, new_value;

      /* Focus tends towards the top-left of the active area, or
       * any fast-moving child.
       */
      focus_on_movement = (ABS (data.fastest_actor.x) > 100) ||
                          (ABS (data.fastest_actor.y) > 100);

      new_value = value = mx_adjustment_get_value (priv->hadjust);
      page_size = mx_adjustment_get_page_size (priv->hadjust);

      if (value + page_size < focus_area.x2)
        new_value = focus_area.x2 - page_size;
      if (value > focus_area.x1)
        new_value = focus_area.x1;
      if (focus_on_movement)
        {
          if (new_value > data.actor_box.x1)
            new_value = data.actor_box.x1;
          else if (new_value + page_size < data.actor_box.x2)
            new_value = data.actor_box.x2 - page_size;
        }
      mx_adjustment_set_value (priv->hadjust, value + (new_value - value)/20.0);

      new_value = value = mx_adjustment_get_value (priv->vadjust);
      page_size = mx_adjustment_get_page_size (priv->vadjust);

      if (value + page_size < focus_area.y2)
        new_value = focus_area.y2 - page_size;
      if (value > focus_area.y1)
        new_value = focus_area.y1;
      if (focus_on_movement)
        {
          if (value > data.actor_box.y1)
            new_value = data.actor_box.y1;
          else if (value + page_size < data.actor_box.y2)
            new_value = data.actor_box.y2 - page_size;
        }
      mx_adjustment_set_value (priv->vadjust, value + (new_value - value)/20.0);
    }

  if (!data.has_dingos && !priv->complete)
    {
      priv->complete = TRUE;
      g_signal_emit (box2d, signals[COMPLETE], 0);
    }
  else if (data.has_dingos && priv->complete)
    priv->complete = FALSE;
}

static void
hw_scene_class_init (HwSceneClass *klass)
{
  GParamSpec *pspec;

  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);
  ClutterBox2DClass *box2d_class = CLUTTER_BOX2D_CLASS (klass);

  g_type_class_add_private (klass, sizeof (HwScenePrivate));

  object_class->get_property = hw_scene_get_property;
  object_class->set_property = hw_scene_set_property;
  object_class->dispose = hw_scene_dispose;
  object_class->finalize = hw_scene_finalize;
  object_class->constructed = hw_scene_constructed;

  actor_class->get_preferred_width = hw_scene_get_preferred_width;
  actor_class->get_preferred_height = hw_scene_get_preferred_height;
  actor_class->allocate = hw_scene_allocate;
  actor_class->apply_transform = hw_scene_apply_transform;
  actor_class->paint = hw_scene_paint;
  actor_class->pick = hw_scene_pick;
  actor_class->map = hw_scene_map;
  actor_class->unmap = hw_scene_unmap;

  box2d_class->iterate = hw_scene_iterate;

  pspec = g_param_spec_boolean ("enable-tracking",
                                "Enable tracking",
                                "Enable movement/focus tracking",
                                FALSE,
                                G_PARAM_STATIC_STRINGS | G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_ENABLE_TRACKING, pspec);

  g_object_class_override_property (object_class,
                                    PROP_HADJUST,
                                    "horizontal-adjustment");

  g_object_class_override_property (object_class,
                                    PROP_VADJUST,
                                    "vertical-adjustment");

  signals[COMPLETE] =
    g_signal_new ("complete",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (HwSceneClass, complete),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__VOID,
                  G_TYPE_NONE, 0);

  signals[EXPLODED] =
    g_signal_new ("exploded",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
                  G_STRUCT_OFFSET (HwSceneClass, exploded),
                  NULL, NULL,
                  hw_marshal_VOID__FLOAT_FLOAT,
                  G_TYPE_NONE, 2, G_TYPE_FLOAT, G_TYPE_FLOAT);
}

static void
hw_scene_notify_size_cb (HwScene *self)
{
  gfloat width, height;
  HwScenePrivate *priv = self->priv;

  clutter_actor_get_preferred_size (CLUTTER_ACTOR (self), NULL, NULL,
                                    &width, &height);
  clutter_actor_set_width (priv->ground, width);
  clutter_actor_set_y (priv->ground,
                       height -
                       clutter_actor_get_height (priv->ground) +
                       GROUND_OFFSET);
}

static void
hw_scene_dingo_collision_cb (ClutterChildMeta      *meta,
                             ClutterBox2DCollision *collision,
                             HwScene               *scene)
{
  const gchar *name;
  ClutterActor *actor, *collidee;

  actor = clutter_child_meta_get_actor (meta);
  collidee = (collision->actor2 == actor) ?
    collision->actor1 : collision->actor2;

  name = clutter_actor_get_name (collidee);

  if (!name)
    return;

  if ((collision->normal_force > 1.0) ||
      ((g_str_equal (name, "Wombat") ||
        g_str_equal (name, "Dingo") ||
        g_str_equal (name, "Slab")) &&
       (collision->normal_force > 0.8)))
    {
      gdouble intensity;
      gfloat x, y, width, height;

      clutter_actor_get_position (actor, &x, &y);
      clutter_actor_get_size (actor, &width, &height);

      intensity = ((gdouble)collision->normal_force) / 2.0;

      x += width / 2;
      y += height / 2;
      hw_utils_explode (CLUTTER_ACTOR (scene), x, y, intensity);

      clutter_actor_destroy (actor);

      g_signal_emit (scene,
                     signals[EXPLODED],
                     g_quark_from_static_string ("dingo"),
                     x, y);
    }
}

static void
hw_scene_generic_collision_cb (ClutterChildMeta      *meta,
                               ClutterBox2DCollision *collision,
                               HwScene               *scene)
{
  if (collision->normal_force > 0.5)
    hw_utils_explode (CLUTTER_ACTOR (scene),
                      collision->position.x,
                      collision->position.y,
                      0.05);
}

static void
hw_scene_actor_added_cb (HwScene      *self,
                         ClutterActor *actor)
{
  ClutterChildMeta *child_meta;
  const gchar *name = clutter_actor_get_name (actor);

  if (!name)
    return;

  child_meta = clutter_container_get_child_meta (CLUTTER_CONTAINER (self),
                                                 actor);

  if (g_str_equal (name, "Dingo"))
    {
      g_signal_connect (child_meta, "collision",
                        G_CALLBACK (hw_scene_dingo_collision_cb), self);
      g_signal_connect (child_meta, "collision",
                        G_CALLBACK (hw_scene_generic_collision_cb), self);
    }
  else if (g_str_equal (name, "Wombat"))
    {
      g_signal_connect (child_meta, "collision",
                        G_CALLBACK (hw_scene_generic_collision_cb), self);
    }
}

static void
hw_scene_notify_simulating_cb (HwScene *self)
{
  GList *c, *children;

  gboolean first = TRUE;
  HwScenePrivate *priv = self->priv;

  if (!clutter_box2d_get_simulating (CLUTTER_BOX2D (self)))
    return;

  priv->focus_area = (ClutterActorBox){ 0, };

  children = clutter_container_get_children (CLUTTER_CONTAINER (self));
  for (c = children; c; c = c->next)
    {
      ClutterActorBox child_box;

      ClutterActor *child = c->data;
      const gchar *name = clutter_actor_get_name (child);

      if (!name ||
          g_str_equal (name, "Launcher") ||
          g_str_equal (name, "Ground"))
        continue;

      clutter_actor_get_allocation_box (child, &child_box);
      if (first)
        {
          priv->focus_area = child_box;
          first = FALSE;
        }
      else
        {
          if (child_box.x1 < priv->focus_area.x1)
            priv->focus_area.x1 = child_box.x1;
          if (child_box.y1 < priv->focus_area.y1)
            priv->focus_area.y1 = child_box.y1;
          if (child_box.x2 > priv->focus_area.x2)
            priv->focus_area.x2 = child_box.x2;
          if (child_box.y2 > priv->focus_area.y2)
            priv->focus_area.y2 = child_box.y2;
        }
    }
  g_list_free (children);
}

static gboolean
hw_scene_captured_event_cb (HwScene            *self,
                            ClutterButtonEvent *event)
{
  if ((event->type == CLUTTER_BUTTON_PRESS) && self->priv->enable_tracking)
    hw_scene_set_enable_tracking (self, FALSE);

  return FALSE;
}

static void
hw_scene_init (HwScene *self)
{
  gint i;

  HwScenePrivate *priv = self->priv = SCENE_PRIVATE (self);
  ClutterActor *actor = CLUTTER_ACTOR (self);

  /* Create background elements */
  priv->sun = clutter_texture_new_from_file ("sun.png", NULL);
  priv->sky = clutter_texture_new_from_file ("sky.png", NULL);
  priv->bgs[0] = clutter_texture_new_from_file ("mountains-1.png", NULL);
  priv->bgs[1] = clutter_texture_new_from_file ("mountains-2.png", NULL);
  priv->bgs[2] = clutter_texture_new_from_file ("hills.png", NULL);

  for (i = 0; i < G_N_ELEMENTS (priv->bgs); i++)
    clutter_texture_set_repeat (CLUTTER_TEXTURE (priv->bgs[i]), TRUE, FALSE);

  clutter_actor_push_internal (actor);
  clutter_actor_set_parent (priv->sun, actor);
  clutter_actor_set_parent (priv->sky, actor);
  clutter_actor_set_parent (priv->bgs[0], actor);
  clutter_actor_set_parent (priv->bgs[1], actor);
  clutter_actor_set_parent (priv->bgs[2], actor);
  clutter_actor_pop_internal (actor);

  /* Set the default size of the scene */
  clutter_actor_set_size (actor, 1280, 600);
  g_signal_connect (self, "notify::natural-width",
                    G_CALLBACK (hw_scene_notify_size_cb), NULL);
  g_signal_connect (self, "notify::natural-height",
                    G_CALLBACK (hw_scene_notify_size_cb), NULL);

  /* Create adjustments */
  priv->hadjust = mx_adjustment_new_with_values (0, 0, 1280, 10, 320, 640);
  priv->vadjust = mx_adjustment_new_with_values (0, 0, 600, 10, 240, 480);

  g_signal_connect_swapped (priv->hadjust, "notify::value",
                            G_CALLBACK (clutter_actor_queue_relayout), self);
  g_signal_connect_swapped (priv->vadjust, "notify::value",
                            G_CALLBACK (clutter_actor_queue_relayout), self);

  /* Create timeline for day-night cycle */
  priv->daynight = clutter_timeline_new (120000);
  g_signal_connect_swapped (priv->daynight, "new-frame",
                            G_CALLBACK (clutter_actor_queue_relayout), self);
  clutter_timeline_set_loop (priv->daynight, TRUE);
  clutter_timeline_start (priv->daynight);

  /* Attach to actor-added to apply rules */
  g_signal_connect (self, "actor-added",
                    G_CALLBACK (hw_scene_actor_added_cb), NULL);

  /* Attach to notify::simulating to calculate focus area */
  g_signal_connect (self, "notify::simulating",
                    G_CALLBACK (hw_scene_notify_simulating_cb), NULL);

  /* Attach to button-press to disable tracking */
  clutter_actor_set_reactive (CLUTTER_ACTOR (self), TRUE);
  g_signal_connect (self, "captured-event",
                    G_CALLBACK (hw_scene_captured_event_cb), NULL);
}

ClutterActor *
hw_scene_new (void)
{
  return g_object_new (HW_TYPE_SCENE, NULL);
}

void
hw_scene_set_size (HwScene *scene, gfloat width, gfloat height)
{
  HwScenePrivate *priv;
  gfloat old_height, difference;

  g_return_if_fail (HW_IS_SCENE (scene));

  priv = scene->priv;

  if (width > 0)
    clutter_actor_set_width (CLUTTER_ACTOR (scene), width);

  hw_scene_get_size (scene, NULL, &old_height);
  difference = height - old_height;

  if (height > 0)
    {
      GList *c, *children;

      height += clutter_actor_get_height (priv->ground) - GROUND_OFFSET;
      clutter_actor_set_height (CLUTTER_ACTOR (scene), height);

      children = clutter_container_get_children (CLUTTER_CONTAINER (scene));
      for (c = children; c; c = c->next)
        {
          gfloat y;

          ClutterActor *child = c->data;

          if (child == priv->ground)
            continue;

          y = clutter_actor_get_y (child);
          clutter_actor_set_y (child, y + difference);
        }
      g_list_free (children);
    }
}

void
hw_scene_get_size (HwScene *scene, gfloat *width, gfloat *height)
{
  HwScenePrivate *priv;

  g_return_if_fail (HW_IS_SCENE (scene));

  priv = scene->priv;

  clutter_actor_get_preferred_size (CLUTTER_ACTOR (scene),
                                    NULL, NULL,
                                    width, height);
  if (height)
    *height -= clutter_actor_get_height (priv->ground) - GROUND_OFFSET;
}

void
hw_scene_set_enable_tracking (HwScene *scene, gboolean enable)
{
  HwScenePrivate *priv;

  g_return_if_fail (HW_IS_SCENE (scene));

  priv = scene->priv;

  if (priv->enable_tracking != enable)
    {
      priv->enable_tracking = enable;
      g_object_notify (G_OBJECT (scene), "enable-tracking");
    }
}
