/* hw-scene.h */

#ifndef _HW_SCENE_H
#define _HW_SCENE_H

#include <glib-object.h>
#include <clutter/clutter.h>
#include <clutter-box2d/clutter-box2d.h>

G_BEGIN_DECLS

#define HW_TYPE_SCENE hw_scene_get_type()

#define HW_SCENE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  HW_TYPE_SCENE, HwScene))

#define HW_SCENE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  HW_TYPE_SCENE, HwSceneClass))

#define HW_IS_SCENE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  HW_TYPE_SCENE))

#define HW_IS_SCENE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  HW_TYPE_SCENE))

#define HW_SCENE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  HW_TYPE_SCENE, HwSceneClass))

typedef struct _HwScene HwScene;
typedef struct _HwSceneClass HwSceneClass;
typedef struct _HwScenePrivate HwScenePrivate;

struct _HwScene
{
  ClutterBox2D parent;

  HwScenePrivate *priv;
};

struct _HwSceneClass
{
  ClutterBox2DClass parent_class;

  void (* complete) (HwScene *scene);
  void (* exploded) (HwScene *scene, gfloat x, gfloat y);
};

GType hw_scene_get_type (void) G_GNUC_CONST;

ClutterActor *hw_scene_new (void);

void hw_scene_set_size (HwScene *scene, gfloat width, gfloat height);
void hw_scene_get_size (HwScene *scene, gfloat *width, gfloat *height);

void hw_scene_set_enable_tracking (HwScene *scene, gboolean enable);
gboolean hw_scene_get_enable_tracking (HwScene *scene);

G_END_DECLS

#endif /* _HW_SCENE_H */
