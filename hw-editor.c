
#include <mx/mx.h>
#include <clutter-box2d/clutter-box2d.h>
#include "hw-launcher.h"
#include "hw-scene.h"
#include "hw-utils.h"
#include <stdlib.h>

typedef struct
{
  gchar        *level_name;
  gchar        *tmp_level_name;
  ClutterActor *box2d;
  GList        *joints;
  ClutterActor *launcher;
  ClutterActor *new_item;
  gfloat        initial_x;
  gfloat        initial_y;
  gboolean      saved;
} HwData;

static ClutterActor *
test_box2d ()
{
  ClutterActor *box2d;

  box2d = hw_scene_new ();
  hw_utils_create_launcher (box2d, 50, 4);

  return box2d;
}

static gboolean
save_scene (HwData *data)
{
  hw_save (data->box2d, data->level_name, data->joints);
  data->saved = TRUE;

  return FALSE;
}

static gboolean
stage_motion_cb (ClutterActor       *stage,
                 ClutterMotionEvent *event,
                 HwData             *data)
{
  gfloat x, y;

  if (clutter_actor_transform_stage_point (data->box2d,
                                           event->x,
                                           event->y,
                                           &x, &y))
    {
      gfloat width, height;

      hw_scene_get_size (HW_SCENE (data->box2d), &width, &height);

      x = MIN (MAX (0, x - data->initial_x),
               width - clutter_actor_get_width (data->new_item));
      y = MIN (MAX (0, y - data->initial_y),
               height - clutter_actor_get_height (data->new_item));

      clutter_actor_set_position (data->new_item, x, y);
    }

  return TRUE;
}

static gboolean
stage_captured_cb (ClutterActor *stage,
                   ClutterEvent *event,
                   HwData       *data)
{
  if ((event->type == CLUTTER_BUTTON_RELEASE) &&
      (((ClutterButtonEvent *)event)->button == 1))
    {
      g_signal_handlers_disconnect_by_func (stage, stage_motion_cb, data);
      g_signal_handlers_disconnect_by_func (stage, stage_captured_cb, data);

      return TRUE;
    }

  return FALSE;
}

static gboolean
item_press_cb (ClutterActor       *item,
               ClutterButtonEvent *event,
               HwData             *data)
{
  ClutterActor *stage = clutter_actor_get_stage (data->box2d);

  if (clutter_box2d_get_simulating (CLUTTER_BOX2D (data->box2d)))
    return FALSE;

  if (event->button == 1)
    {
      data->new_item = item;

      clutter_set_motion_events_enabled (FALSE);

      g_signal_connect (stage, "motion-event",
                        G_CALLBACK (stage_motion_cb), data);
      g_signal_connect (stage, "captured-event",
                        G_CALLBACK (stage_captured_cb), data);

      clutter_actor_transform_stage_point (item,
                                           event->x,
                                           event->y,
                                           &data->initial_x,
                                           &data->initial_y);
    }
  else
    {
      MxMenu *menu = g_object_get_data (G_OBJECT (item), "menu");

      if (!clutter_actor_get_parent (CLUTTER_ACTOR (menu)))
        clutter_container_add_actor (CLUTTER_CONTAINER (stage),
                                     CLUTTER_ACTOR (menu));

      mx_menu_show_with_position (menu, event->x, event->y);
    }

  return TRUE;
}

static void
remove_cb (MxAction *action, ClutterActor *item)
{
  clutter_actor_destroy (item);
}

static void
disable_cb (MxAction *action, ClutterActor *item)
{
  ClutterActor *parent = clutter_actor_get_parent (item);

  if (!parent)
    return;

  clutter_container_child_set (CLUTTER_CONTAINER (parent), item,
                               "mode", CLUTTER_BOX2D_NONE, NULL);
}

static void
static_cb (MxAction *action, ClutterActor *item)
{
  ClutterActor *parent = clutter_actor_get_parent (item);

  if (!parent)
    return;

  clutter_container_child_set (CLUTTER_CONTAINER (parent), item,
                               "mode", CLUTTER_BOX2D_STATIC, NULL);
}

static void
dynamic_cb (MxAction *action, ClutterActor *item)
{
  ClutterActor *parent = clutter_actor_get_parent (item);

  if (!parent)
    return;

  clutter_container_child_set (CLUTTER_CONTAINER (parent), item,
                               "mode", CLUTTER_BOX2D_DYNAMIC, NULL);
}

static void
item_add_menu (HwData *data, ClutterActor *item)
{
  MxAction *action;
  ClutterActor *menu;

  menu = mx_menu_new ();
  action = mx_action_new_full ("remove",
                               "Remove",
                               G_CALLBACK (remove_cb),
                               item);
  mx_menu_add_action (MX_MENU (menu), action);

  action = mx_action_new_full ("disable",
                               "Disable physics",
                               G_CALLBACK (disable_cb),
                               item);
  mx_menu_add_action (MX_MENU (menu), action);

  action = mx_action_new_full ("static",
                               "Make static",
                               G_CALLBACK (static_cb),
                               item);
  mx_menu_add_action (MX_MENU (menu), action);

  action = mx_action_new_full ("dynamic",
                               "Make dynamic",
                               G_CALLBACK (dynamic_cb),
                               item);
  mx_menu_add_action (MX_MENU (menu), action);

  g_object_set_data (G_OBJECT (item), "menu", menu);

  clutter_actor_hide (menu);
}

static gboolean
new_item_press_cb (ClutterActor       *item,
                   ClutterButtonEvent *event,
                   HwData             *data)
{
  gchar *file = g_object_get_data (G_OBJECT (item), "file");
  MxTextureCache *cache = mx_texture_cache_get_default ();

  if (event->button != 1)
    return FALSE;

  data->new_item = (ClutterActor *)mx_texture_cache_get_texture (cache, file);
  clutter_container_add_actor (CLUTTER_CONTAINER (data->box2d), data->new_item);

  item_add_menu (data, data->new_item);

  g_object_set_data (G_OBJECT (data->new_item), "image", file);

  if (g_str_has_prefix (file, "dingo"))
    {
      clutter_actor_set_name (data->new_item, "Dingo");
      clutter_container_child_set (CLUTTER_CONTAINER (data->box2d),
                                   data->new_item, "is-circle", TRUE, NULL);
    }
  else if (g_str_has_prefix (file, "wood"))
    clutter_actor_set_name (data->new_item, "Plank");
  else if (g_str_has_prefix (file, "stone"))
    clutter_actor_set_name (data->new_item, "Slab");
  else
    clutter_actor_set_name (data->new_item, "Box");

  clutter_container_child_set (CLUTTER_CONTAINER (data->box2d), data->new_item,
                               "mode", CLUTTER_BOX2D_DYNAMIC, NULL);

  clutter_actor_set_reactive (data->new_item, TRUE);
  g_signal_connect (data->new_item, "button-press-event",
                    G_CALLBACK (item_press_cb), data);
  item_press_cb (data->new_item, event, data);

  clutter_actor_transform_stage_point (item,
                                       event->x,
                                       event->y,
                                       &data->initial_x,
                                       &data->initial_y);

  return TRUE;
}

static void
populate_grid (HwData *data, ClutterContainer *container)
{
  GDir *dir;
  const gchar *file;

  if (!(dir = g_dir_open ("./", 0, NULL)))
    return;

  while ((file = g_dir_read_name (dir)))
    {
      if (!g_file_test (file, G_FILE_TEST_IS_REGULAR))
        continue;

      if (!g_str_has_suffix (file, ".png"))
        continue;

      if (g_str_has_prefix (file, "dingo") ||
          g_str_has_prefix (file, "wood") ||
          g_str_has_prefix (file, "stone"))
        {
          ClutterActor *texture = clutter_texture_new_from_file (file, NULL);

          g_object_set_data (G_OBJECT (texture), "file", g_strdup (file));
          clutter_container_add_actor (container, texture);

          /* Hook up to place objects */
          clutter_actor_set_reactive (texture, TRUE);
          g_signal_connect (texture, "button-press-event",
                            G_CALLBACK (new_item_press_cb), data);
        }
    }

  g_dir_close (dir);
}

static void
get_launcher (HwData *data)
{
  data->launcher =
    clutter_container_find_child_by_name (CLUTTER_CONTAINER (data->box2d),
                                          "Launcher");
  if (!data->launcher)
    data->launcher = hw_utils_create_launcher (data->box2d, 50, 4);
  clutter_actor_set_reactive (data->launcher, FALSE);
}

static void
load_level (HwData *data)
{
  GList *c, *children;

  if (data->box2d)
    clutter_actor_destroy (data->box2d);

  g_list_foreach (data->joints,
                  (GFunc)hw_joint_free,
                  GINT_TO_POINTER (TRUE));
  g_list_free (data->joints);
  data->joints = NULL;

  data->box2d = hw_load (data->level_name, &data->joints);

  /* Make actors moveable */
  children = clutter_container_get_children (CLUTTER_CONTAINER (data->box2d));
  for (c = children; c; c = c->next)
    {
      ClutterActor *child = c->data;
      const gchar *name = clutter_actor_get_name (child);

      if (!name)
        continue;

      if (g_str_equal (name, "Ground") ||
          g_str_equal (name, "Launcher"))
        continue;

      item_add_menu (data, child);

      clutter_actor_set_reactive (child, TRUE);
      g_signal_connect (child, "button-press-event",
                        G_CALLBACK (item_press_cb), data);
    }
  g_list_free (children);
}

static void
play_toggled_cb (MxButton *button, GParamSpec *pspec, HwData *data)
{
  gchar *level_name = data->level_name;
  ClutterActor *parent = clutter_actor_get_parent (data->box2d);

  if (mx_button_get_toggled (button))
    {
      /* Save level to temporary location */
      data->level_name = data->tmp_level_name;
      hw_save (data->box2d, data->level_name, data->joints);
      data->saved = FALSE;

      /* Start simulating */
      clutter_actor_set_reactive (data->launcher, TRUE);
      clutter_box2d_set_simulating (CLUTTER_BOX2D (data->box2d), TRUE);
    }
  else
    {
      /* Load level from temporary location, unless saved */
      if (!data->saved)
        data->level_name = data->tmp_level_name;

      load_level (data);
      clutter_container_add_actor (CLUTTER_CONTAINER (parent), data->box2d);
      get_launcher (data);
    }
  data->level_name = level_name;
}

static void
set_width_cb (ClutterText *entry, HwData *data)
{
  gfloat width = atoi (clutter_text_get_text (entry));

  if (width <= 0)
    return;

  hw_scene_set_size (HW_SCENE (data->box2d), width, 0);
}

static void
set_height_cb (ClutterText *entry, HwData *data)
{
  gfloat height = atoi (clutter_text_get_text (entry));

  if (height <= 0)
    return;

  hw_scene_set_size (HW_SCENE (data->box2d), 0, height);
  clutter_actor_destroy (data->launcher);
  get_launcher (data);
}

static void
set_ammo_cb (ClutterText *entry, HwData *data)
{
  gint ammo = atoi (clutter_text_get_text (entry));

  if (ammo > 0)
    hw_launcher_set_ammo (HW_LAUNCHER (data->launcher), ammo);
}

int
main (int argc, char **argv)
{
  HwData data = { 0, };
  gchar *text;
  MxWindow *window;
  MxToolbar *toolbar;
  MxApplication *app;
  gfloat width, height;
  ClutterActor *stage, *scroll, *hbox, *scrollview, *grid, *vbox, *table,
               *label, *entry, *button, *icon;

  app = mx_application_new (&argc, &argv, "Happy Wombats Editor",
                            MX_APPLICATION_SINGLE_INSTANCE);
  window = mx_application_create_window (app);
  stage = (ClutterActor *)mx_window_get_clutter_stage (window);

  mx_style_load_from_file (mx_style_get_default (), "hw.css", NULL);

  scroll = mx_kinetic_scroll_view_new ();
  mx_kinetic_scroll_view_set_deceleration (MX_KINETIC_SCROLL_VIEW (scroll), 5);

  data.joints = NULL;
  if (argc > 1)
    data.level_name = argv[1];
  else
    data.level_name = "default_scene.ini";

  data.tmp_level_name = g_strdup_printf ("%s%stmp-level.ini",
                                         g_get_tmp_dir (),
                                         G_DIR_SEPARATOR_S);

  if (g_file_test (data.level_name, G_FILE_TEST_IS_REGULAR))
    load_level (&data);
  else
    data.box2d = test_box2d ();
  clutter_container_add_actor (CLUTTER_CONTAINER (scroll), data.box2d);

  get_launcher (&data);

  /* Create editing interface */
  hbox = mx_box_layout_new ();
  vbox = mx_box_layout_new ();
  mx_box_layout_set_orientation (MX_BOX_LAYOUT (vbox), MX_ORIENTATION_VERTICAL);

  clutter_container_add (CLUTTER_CONTAINER (hbox), vbox, scroll, NULL);
  clutter_container_child_set (CLUTTER_CONTAINER (hbox), scroll,
                               "expand", TRUE, NULL);

  /* Create objects palette */
  scrollview = mx_scroll_view_new ();
  mx_scroll_view_set_scroll_policy (MX_SCROLL_VIEW (scrollview),
                                    MX_SCROLL_POLICY_VERTICAL);
  grid = mx_grid_new ();
  mx_grid_set_max_stride (MX_GRID (grid), 1);
  populate_grid (&data, CLUTTER_CONTAINER (grid));
  clutter_container_add_actor (CLUTTER_CONTAINER (scrollview), grid);

  /* Create properties table */
  table = mx_table_new ();
  label = mx_label_new_with_text ("Level parameters");
  mx_table_add_actor_with_properties (MX_TABLE (table), label,
                                      0, 0,
                                      "column-span", 3,
                                      "x-expand", FALSE,
                                      "x-fill", FALSE,
                                      "x-align", MX_ALIGN_START,
                                      NULL);

  hw_scene_get_size (HW_SCENE (data.box2d), &width, &height);
  text = g_strdup_printf ("%d", (gint)width);
  entry = mx_entry_new_with_text (text);
  g_free (text);
  mx_table_add_actor (MX_TABLE (table), entry, 1, 0);
  g_signal_connect (mx_entry_get_clutter_text (MX_ENTRY (entry)), "activate",
                    G_CALLBACK (set_width_cb), &data);

  label = mx_label_new_with_text ("X");
  mx_table_add_actor_with_properties (MX_TABLE (table), label,
                                      1, 1,
                                      "x-expand", FALSE,
                                      "x-fill", FALSE,
                                      "y-fill", FALSE,
                                      NULL);

  text = g_strdup_printf ("%d", (gint)height);
  entry = mx_entry_new_with_text (text);
  g_free (text);
  mx_table_add_actor (MX_TABLE (table), entry, 1, 2);
  g_signal_connect (mx_entry_get_clutter_text (MX_ENTRY (entry)), "activate",
                    G_CALLBACK (set_height_cb), &data);

  label = mx_label_new_with_text ("Ammo");
  mx_table_add_actor_with_properties (MX_TABLE (table), label,
                                      2, 0,
                                      "x-expand", FALSE,
                                      "x-fill", FALSE,
                                      "x-align", MX_ALIGN_START,
                                      NULL);

  text = g_strdup_printf ("%u", hw_launcher_get_ammo (
                                  HW_LAUNCHER (data.launcher)));
  entry = mx_entry_new_with_text (text);
  g_free (text);
  mx_table_add_actor_with_properties (MX_TABLE (table), entry,
                                      2, 1,
                                      "column-span", 2,
                                      NULL);
  g_signal_connect (mx_entry_get_clutter_text (MX_ENTRY (entry)), "activate",
                    G_CALLBACK (set_ammo_cb), &data);

  /* Pack palette/properties panel into vbox */
  clutter_container_add (CLUTTER_CONTAINER (vbox), table, scrollview, NULL);
  clutter_container_child_set (CLUTTER_CONTAINER (vbox), scrollview,
                               "expand", TRUE, NULL);

  mx_window_set_child (window, hbox);

  /* Create toolbar buttons */
  hbox = mx_box_layout_new ();

  button = mx_button_new ();
  mx_button_set_is_toggle (MX_BUTTON (button), TRUE);
  g_signal_connect (button, "notify::toggled",
                    G_CALLBACK (play_toggled_cb), &data);
  icon = mx_icon_new ();
  mx_icon_set_icon_size (MX_ICON (icon), 16);
  mx_icon_set_icon_name (MX_ICON (icon), "media-playback-start");
  mx_bin_set_child (MX_BIN (button), icon);

  clutter_container_add_actor (CLUTTER_CONTAINER (hbox), button);

  button = mx_button_new ();
  g_signal_connect_swapped (button, "clicked",
                            G_CALLBACK (save_scene), &data);
  icon = mx_icon_new ();
  mx_icon_set_icon_size (MX_ICON (icon), 16);
  mx_icon_set_icon_name (MX_ICON (icon), "document-save");
  mx_bin_set_child (MX_BIN (button), icon);

  clutter_container_add_actor (CLUTTER_CONTAINER (hbox), button);

  /* Add to toolbar */
  toolbar = mx_window_get_toolbar (window);
  mx_bin_set_child (MX_BIN (toolbar), hbox);
  mx_bin_set_fill (MX_BIN (toolbar), FALSE, TRUE);
  mx_bin_set_alignment (MX_BIN (toolbar), MX_ALIGN_START, MX_ALIGN_MIDDLE);

  clutter_actor_set_size (stage, 1024, 600);
  clutter_actor_show (stage);

  mx_application_run (app);

  hw_utils_shutdown ();

  return 0;
}

