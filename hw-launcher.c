/* hw-launcher.c */

#include "hw-launcher.h"
#include "hw-marshal.h"
#include "hw-scene.h"
#include "hw-utils.h"
#include <mx/mx.h>
#include <clutter-box2d/clutter-box2d.h>
#include <math.h>

G_DEFINE_TYPE (HwLauncher, hw_launcher, CLUTTER_TYPE_ACTOR)

#define LAUNCHER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), HW_TYPE_LAUNCHER, HwLauncherPrivate))

enum
{
  LAUNCH,

  LAST_SIGNAL
};

enum
{
  PROP_0,

  PROP_AMMO
};

struct _HwLauncherPrivate
{
  guint            ammo;

  ClutterActor    *ammo_layout;
  ClutterActor    *cannon_base;
  ClutterActor    *cannon;
  ClutterActor    *power_meter;
  MxProgressBar   *power_bar;
  ClutterActor    *angle_slider;

  ClutterTimeline *power_timeline;
};

static guint signals[LAST_SIGNAL] = { 0, };

#define CANNON_PIVOT_X 20
#define CANNON_PIVOT_Y 31
#define CANNON_OFFSET_X 24
#define CANNON_OFFSET_Y 30

static void
hw_launcher_get_property (GObject    *object,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  HwLauncherPrivate *priv = HW_LAUNCHER (object)->priv;

  switch (property_id)
    {
    case PROP_AMMO:
      g_value_set_uint (value, priv->ammo);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
hw_launcher_set_property (GObject      *object,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  HwLauncher *self = HW_LAUNCHER (object);

  switch (property_id)
    {
    case PROP_AMMO:
      hw_launcher_set_ammo (self, g_value_get_uint (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
hw_launcher_dispose (GObject *object)
{
  HwLauncherPrivate *priv = HW_LAUNCHER (object)->priv;

  if (priv->ammo_layout)
    {
      clutter_actor_destroy (priv->ammo_layout);
      priv->ammo_layout = NULL;
    }

  if (priv->cannon_base)
    {
      clutter_actor_destroy (priv->cannon_base);
      priv->cannon_base = NULL;
    }

  if (priv->cannon)
    {
      clutter_actor_destroy (priv->cannon);
      priv->cannon = NULL;
    }

  if (priv->power_meter)
    {
      clutter_actor_destroy (priv->power_meter);
      priv->power_meter = NULL;
    }

  if (priv->angle_slider)
    {
      clutter_actor_destroy (priv->angle_slider);
      priv->angle_slider = NULL;
    }

  if (priv->power_timeline)
    {
      g_object_unref (priv->power_timeline);
      priv->power_timeline = NULL;
    }

  G_OBJECT_CLASS (hw_launcher_parent_class)->dispose (object);
}

static void
hw_launcher_finalize (GObject *object)
{
  G_OBJECT_CLASS (hw_launcher_parent_class)->finalize (object);
}

static void
hw_launcher_get_preferred_width (ClutterActor *actor,
                                 gfloat        for_height,
                                 gfloat       *min_width_p,
                                 gfloat       *nat_width_p)
{
  gfloat width;

  HwLauncherPrivate *priv = HW_LAUNCHER (actor)->priv;

  width = clutter_actor_get_width (priv->cannon_base) +
          clutter_actor_get_width (priv->ammo_layout) + 10;

  if (min_width_p)
    *min_width_p = width;
  if (nat_width_p)
    *nat_width_p = width;
}

static void
hw_launcher_get_preferred_height (ClutterActor *actor,
                                  gfloat        for_width,
                                  gfloat       *min_height_p,
                                  gfloat       *nat_height_p)
{
  gfloat height;

  HwLauncherPrivate *priv = HW_LAUNCHER (actor)->priv;

  height = clutter_actor_get_height (priv->cannon) +
           clutter_actor_get_height (priv->cannon_base) -
           CANNON_PIVOT_Y - CANNON_OFFSET_Y;

  if (min_height_p)
    *min_height_p = height;
  if (nat_height_p)
    *nat_height_p = height;
}

static void
hw_launcher_allocate (ClutterActor           *actor,
                      const ClutterActorBox  *box,
                      ClutterAllocationFlags  flags)
{
  ClutterActorBox child_box;

  HwLauncherPrivate *priv = HW_LAUNCHER (actor)->priv;

  CLUTTER_ACTOR_CLASS (hw_launcher_parent_class)->allocate (actor, box, flags);

  child_box.x1 = ((clutter_actor_get_width (priv->cannon) -
                   clutter_actor_get_width (priv->power_meter)) / 2.f) -
                 CANNON_PIVOT_X - CANNON_OFFSET_X;
  child_box.y1 = 0;
  child_box.x2 = child_box.x1 + clutter_actor_get_width (priv->power_meter);
  child_box.y2 = clutter_actor_get_height (priv->power_meter);
  clutter_actor_allocate (priv->power_meter, &child_box, flags);

  child_box.x1 = -CANNON_PIVOT_X - CANNON_OFFSET_X;
  child_box.x2 = child_box.x1 + clutter_actor_get_width (priv->cannon);
  child_box.y2 = clutter_actor_get_height (priv->cannon);
  clutter_actor_allocate (priv->cannon, &child_box, flags);

  child_box.y1 = child_box.y2 -
                 CANNON_PIVOT_Y - CANNON_OFFSET_Y;
  child_box.y2 = child_box.y1 + clutter_actor_get_height (priv->cannon_base);
  child_box.x1 = (child_box.x2 - clutter_actor_get_width (priv->cannon_base)) /
                 2.f;
  child_box.x2 = child_box.x1 + clutter_actor_get_width (priv->cannon_base);
  clutter_actor_allocate (priv->cannon_base, &child_box, flags);

  child_box.x1 = child_box.x2 + 10;
  child_box.x2 = child_box.x1 + clutter_actor_get_width (priv->ammo_layout);
  child_box.y1 = child_box.y2 - clutter_actor_get_height (priv->ammo_layout);
  clutter_actor_allocate (priv->ammo_layout, &child_box, flags);

  child_box.y1 = child_box.y2 + 10;
  child_box.y2 = child_box.y1 + clutter_actor_get_height (priv->angle_slider);
  child_box.x1 = 0;
  child_box.x2 = clutter_actor_get_width (priv->angle_slider);
  clutter_actor_allocate (priv->angle_slider, &child_box, flags);
}

static void
hw_launcher_paint (ClutterActor *actor)
{
  HwLauncherPrivate *priv = HW_LAUNCHER (actor)->priv;

  clutter_actor_paint (priv->ammo_layout);
  clutter_actor_paint (priv->cannon);
  clutter_actor_paint (priv->cannon_base);
  clutter_actor_paint (priv->angle_slider);
  clutter_actor_paint (priv->power_meter);
}

static void
hw_launcher_pick (ClutterActor *actor, const ClutterColor *color)
{
  if (CLUTTER_ACTOR_IS_REACTIVE (actor))
    {
      HwLauncherPrivate *priv = HW_LAUNCHER (actor)->priv;

      clutter_actor_paint (priv->cannon);
      clutter_actor_paint (priv->angle_slider);
    }
}

static void
hw_launcher_map (ClutterActor *actor)
{
  HwLauncherPrivate *priv = HW_LAUNCHER (actor)->priv;

  CLUTTER_ACTOR_CLASS (hw_launcher_parent_class)->map (actor);

  clutter_actor_map (priv->ammo_layout);
  clutter_actor_map (priv->cannon_base);
  clutter_actor_map (priv->cannon);
  clutter_actor_map (priv->power_meter);
  clutter_actor_map (priv->angle_slider);
}

static void
hw_launcher_unmap (ClutterActor *actor)
{
  HwLauncherPrivate *priv = HW_LAUNCHER (actor)->priv;

  clutter_actor_unmap (priv->ammo_layout);
  clutter_actor_unmap (priv->cannon_base);
  clutter_actor_unmap (priv->cannon);
  clutter_actor_unmap (priv->power_meter);
  clutter_actor_unmap (priv->angle_slider);

  CLUTTER_ACTOR_CLASS (hw_launcher_parent_class)->unmap (actor);
}

static void
hw_launcher_class_init (HwLauncherClass *klass)
{
  GParamSpec *pspec;

  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);

  g_type_class_add_private (klass, sizeof (HwLauncherPrivate));

  object_class->get_property = hw_launcher_get_property;
  object_class->set_property = hw_launcher_set_property;
  object_class->dispose = hw_launcher_dispose;
  object_class->finalize = hw_launcher_finalize;

  actor_class->get_preferred_width = hw_launcher_get_preferred_width;
  actor_class->get_preferred_height = hw_launcher_get_preferred_height;
  actor_class->allocate = hw_launcher_allocate;
  actor_class->paint = hw_launcher_paint;
  actor_class->pick = hw_launcher_pick;
  actor_class->map = hw_launcher_map;
  actor_class->unmap = hw_launcher_unmap;

  pspec = g_param_spec_uint ("ammo",
                             "Ammo",
                             "Ammunition left",
                             0, G_MAXUINT, 3,
                             G_PARAM_STATIC_STRINGS | G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_AMMO, pspec);

  signals[LAUNCH] =
    g_signal_new ("launch",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (HwLauncherClass, launch),
                  NULL, NULL,
                  hw_marshal_VOID__DOUBLE_DOUBLE,
                  G_TYPE_NONE, 2, G_TYPE_DOUBLE, G_TYPE_DOUBLE);
}

static void
hw_launcher_notify_value_cb (MxSlider   *slider,
                             GParamSpec *pspec,
                             HwLauncher *self)
{
  HwLauncherPrivate *priv = self->priv;

  clutter_actor_set_rotation (priv->cannon,
                              CLUTTER_Z_AXIS,
                              mx_slider_get_value (slider) * 90,
                              (clutter_actor_get_width (priv->cannon) / 2) +
                              CANNON_PIVOT_X,
                              clutter_actor_get_height (priv->cannon) -
                                CANNON_PIVOT_Y,
                              0);
  clutter_actor_set_rotation (priv->power_meter,
                              CLUTTER_Z_AXIS,
                              mx_slider_get_value (slider) * 90,
                              (clutter_actor_get_width (priv->power_meter) /
                               2) + CANNON_PIVOT_X,
                              clutter_actor_get_height (priv->cannon) -
                                CANNON_PIVOT_Y,
                              0);
}

static gboolean
hw_launcher_button_release_cb (ClutterActor       *cannon,
                               ClutterButtonEvent *event,
                               HwLauncher         *self)
{
  HwLauncherPrivate *priv = self->priv;

  if ((!event || (event->button == 1)) &&
      (clutter_timeline_is_playing (priv->power_timeline)))
    {
      gfloat x, y, mag;
      gdouble angle, power;
      MxTextureCache *cache;
      ClutterActor *wombat, *parent;
      ClutterVertex launch, origin_out;

      ClutterVertex origin_in = { 0, };

      clutter_actor_animate (priv->power_meter, CLUTTER_EASE_OUT_QUAD, 150,
                             "opacity", 0x00, NULL);
      clutter_timeline_pause (priv->power_timeline);

      angle = mx_slider_get_value (MX_SLIDER (priv->angle_slider)) * G_PI_2;
      power = clutter_timeline_get_progress (priv->power_timeline);
      g_signal_emit (self, signals[LAUNCH], 0, angle, power);

      if (priv->ammo)
        hw_launcher_set_ammo (self, priv->ammo - 1);

      parent = clutter_actor_get_parent (CLUTTER_ACTOR (self));
      if (!parent || !HW_IS_SCENE (parent))
        return TRUE;

      /* Launch a wombat */
      cache = mx_texture_cache_get_default ();
      wombat = (ClutterActor *)mx_texture_cache_get_texture (cache,
                                                             "wombat.png");

      g_object_set_data (G_OBJECT (wombat), "image", "wombat.png");
      clutter_actor_set_name (wombat, "Wombat");

      /* Work out the launch vector */
      x = sin (angle);
      y = - cos (angle);

      mag = sqrt (pow (x, 2) + pow (y, 2));
      launch.x = (x / mag) * power * 1200.f;
      launch.y = (y / mag) * power * 1200.f;
      launch.z = 0;

      /* Work out the launch origin */
      origin_in.x += clutter_actor_get_width (wombat) / 2;
      origin_in.y -= clutter_actor_get_height (wombat) / 2;
      clutter_actor_apply_relative_transform_to_point (priv->cannon,
                                                       parent,
                                                       &origin_in,
                                                       &origin_out);
      clutter_actor_set_position (wombat,
                                  origin_out.x - origin_in.x,
                                  origin_out.y + origin_in.y);

      clutter_container_add_actor (CLUTTER_CONTAINER (parent), wombat);
      clutter_container_child_set (CLUTTER_CONTAINER (parent), wombat,
                                   "is-circle", TRUE,
                                   "mode", CLUTTER_BOX2D_DYNAMIC,
                                   "linear-velocity", &launch,
                                   NULL);

      /* Explosion! */
      hw_utils_explode (parent, origin_out.x, origin_out.y, 1.0);

      /* Enable tracking */
      hw_scene_set_enable_tracking (HW_SCENE (parent), TRUE);

      return TRUE;
    }

  return FALSE;
}

static gboolean
hw_launcher_button_press_cb (ClutterActor       *cannon,
                             ClutterButtonEvent *event,
                             HwLauncher         *self)
{
  HwLauncherPrivate *priv = self->priv;

  if ((!event) || (event->button == 1))
    {
      if (clutter_timeline_is_playing (priv->power_timeline))
        return hw_launcher_button_release_cb (cannon, event, self);

      if (!priv->ammo)
        return TRUE;

      mx_progress_bar_set_progress (priv->power_bar, 0.0);
      clutter_actor_animate (priv->power_meter, CLUTTER_EASE_OUT_QUAD, 150,
                             "opacity", 0xff, NULL);
      clutter_timeline_rewind (priv->power_timeline);
      clutter_timeline_start (priv->power_timeline);

      return TRUE;
    }

  return FALSE;
}

static void
hw_launcher_new_frame_cb (ClutterTimeline *timeline,
                          gint             frame_num,
                          HwLauncher      *self)
{
  HwLauncherPrivate *priv = self->priv;

  mx_progress_bar_set_progress (priv->power_bar,
                                clutter_timeline_get_progress (timeline));
}

static void
hw_launcher_completed_cb (ClutterTimeline *timeline,
                          HwLauncher      *self)
{
  HwLauncherPrivate *priv = self->priv;
  clutter_actor_animate (priv->power_meter, CLUTTER_EASE_OUT_QUAD, 150,
                         "opacity", 0x00, NULL);
}

static gboolean
hw_launcher_key_press_cb (HwLauncher      *self,
                          ClutterKeyEvent *event)
{
  HwLauncherPrivate *priv = self->priv;

  switch (event->keyval)
    {
    case CLUTTER_Left :
      {
        MxSlider *slider = MX_SLIDER (priv->angle_slider);
        gdouble value = mx_slider_get_value (slider) - 0.03;
        mx_slider_set_value (slider, MAX (0.0, value));
      }
      return TRUE;

    case CLUTTER_Right :
      {
        MxSlider *slider = MX_SLIDER (priv->angle_slider);
        gdouble value = mx_slider_get_value (slider) + 0.03;
        mx_slider_set_value (slider, MIN (1.0, value));
      }
      return TRUE;

    case CLUTTER_Return :
    case CLUTTER_space :
      return hw_launcher_button_press_cb (priv->cannon, NULL, self);
    }

  return FALSE;
}

static void
hw_launcher_init (HwLauncher *self)
{
  ClutterActor *meter;

  ClutterActor *actor = CLUTTER_ACTOR (self);
  HwLauncherPrivate *priv = self->priv = LAUNCHER_PRIVATE (self);

  priv->ammo_layout = mx_box_layout_new ();
  mx_box_layout_set_enable_animations (MX_BOX_LAYOUT (priv->ammo_layout),
                                       TRUE);

  priv->cannon_base = clutter_texture_new_from_file ("cannon-base.png", NULL);
  priv->cannon = clutter_texture_new_from_file ("cannon.png", NULL);
  priv->power_meter = clutter_group_new ();
  meter = mx_progress_bar_new ();
  priv->power_bar = MX_PROGRESS_BAR (meter);
  clutter_container_add_actor (CLUTTER_CONTAINER (priv->power_meter), meter);
  clutter_actor_set_opacity (priv->power_meter, 0x00);
  priv->angle_slider = mx_slider_new ();

  clutter_actor_set_rotation (meter, CLUTTER_Z_AXIS, -90, 0, 0, 0);
  clutter_actor_set_size (priv->power_meter,
                          clutter_actor_get_height (meter),
                          clutter_actor_get_width (meter));

  clutter_actor_set_parent (priv->ammo_layout, actor);
  clutter_actor_set_parent (priv->cannon_base, actor);
  clutter_actor_set_parent (priv->cannon, actor);
  clutter_actor_set_parent (priv->power_meter, actor);
  clutter_actor_set_parent (priv->angle_slider, actor);

  g_signal_connect (priv->angle_slider, "notify::value",
                    G_CALLBACK (hw_launcher_notify_value_cb), self);
  mx_slider_set_value (MX_SLIDER (priv->angle_slider), 0.5);

  clutter_actor_set_reactive (priv->cannon, TRUE);
  g_signal_connect (priv->cannon, "button-press-event",
                    G_CALLBACK (hw_launcher_button_press_cb), self);
  g_signal_connect (priv->cannon, "button-release-event",
                    G_CALLBACK (hw_launcher_button_release_cb), self);

  hw_launcher_set_ammo (self, 3);

  priv->power_timeline = clutter_timeline_new (1000);
  clutter_timeline_set_delay (priv->power_timeline, 150);
  g_signal_connect (priv->power_timeline, "new-frame",
                    G_CALLBACK (hw_launcher_new_frame_cb), self);
  g_signal_connect (priv->power_timeline, "completed",
                    G_CALLBACK (hw_launcher_completed_cb), self);

  clutter_actor_set_reactive (CLUTTER_ACTOR (self), TRUE);
  g_signal_connect (self, "key-press-event",
                    G_CALLBACK (hw_launcher_key_press_cb), NULL);
}

ClutterActor *
hw_launcher_new (void)
{
  return g_object_new (HW_TYPE_LAUNCHER, NULL);
}

void
hw_launcher_set_ammo (HwLauncher *launcher, guint rounds)
{
  HwLauncherPrivate *priv;

  g_return_if_fail (HW_IS_LAUNCHER (launcher));

  priv = launcher->priv;
  if (priv->ammo != rounds)
    {
      gint i;
      GList *c, *children =
        clutter_container_get_children (CLUTTER_CONTAINER (priv->ammo_layout));

      priv->ammo = rounds;

      if (rounds)
        rounds --;

      children = g_list_reverse (children);

      for (i = 0, c = children; c; c = c->next, i++)
        if ((i + 1) > rounds)
          clutter_container_remove_actor (
            CLUTTER_CONTAINER (priv->ammo_layout), CLUTTER_ACTOR (c->data));

      g_list_free (children);

      for (; i < rounds; i++)
        {
          ClutterActor *child = clutter_texture_new_from_file ("wombat.png",
                                                               NULL);
          clutter_container_add_actor (CLUTTER_CONTAINER (priv->ammo_layout),
                                       child);
        }

      g_object_notify (G_OBJECT (launcher), "ammo");
    }
}

guint
hw_launcher_get_ammo (HwLauncher *launcher)
{
  g_return_val_if_fail (HW_IS_LAUNCHER (launcher), 0);
  return launcher->priv->ammo;
}
