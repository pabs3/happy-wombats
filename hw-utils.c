
#include "hw-utils.h"
#include "hw-circle.h"
#include "hw-launcher.h"
#include "hw-scene.h"
#include <clutter-box2d/clutter-box2d.h>
#include <mx/mx.h>
#include <stdlib.h>
#include <math.h>
#include <canberra.h>

static void
hw_key_file_set_vertex (GKeyFile      *key_file,
                        const gchar   *group,
                        const gchar   *key,
                        ClutterVertex *vertex)
{
  gchar *vertex_string = g_strdup_printf ("%f,%f,%f",
                                          vertex->x,
                                          vertex->y,
                                          vertex->z);
  g_key_file_set_string (key_file, group, key, vertex_string);
  g_free (vertex_string);
}

void
hw_save (ClutterActor *box2d, const gchar *path, GList *joints)
{
  gchar *data;
  GList *c, *j;
  GHashTable *actor_count;

  GList *children = clutter_container_get_children (CLUTTER_CONTAINER (box2d));
  GKeyFile *key_file = g_key_file_new ();

  actor_count = g_hash_table_new_full (g_str_hash, g_str_equal,
                                       (GDestroyNotify)g_free, NULL);

  for (c = children; c; c = c->next)
    {
      gint type;
      guint count;
      gboolean reactive, circle;

      ClutterActor *child = c->data;
      gchar *group = (gchar *)clutter_actor_get_name (child);

      if (HW_IS_LAUNCHER (child))
        {
          gfloat x;
          guint ammo;

          x = clutter_actor_get_x (child);
          ammo = hw_launcher_get_ammo (HW_LAUNCHER (child));

          g_key_file_set_double (key_file, "Launcher", "x", x);
          g_key_file_set_integer (key_file, "Launcher", "ammo", ammo);

          continue;
        }

      if (!group)
        continue;

      if (g_str_equal (group, "Ground"))
        {
          gfloat width, height;

          hw_scene_get_size (HW_SCENE (box2d), &width, &height);

          g_key_file_set_double (key_file, group, "width", width);
          g_key_file_set_double (key_file, group, "height", height);

          continue;
        }

      count = GPOINTER_TO_UINT (g_hash_table_lookup (actor_count, group)) + 1;
      g_hash_table_replace (actor_count,
                            g_strdup (group),
                            GUINT_TO_POINTER (count));
      group = g_strdup_printf ("%s%d", group, count);

      clutter_container_child_get (CLUTTER_CONTAINER (box2d), child,
                                   "is-circle", &circle, NULL);

      g_key_file_set_string (key_file,
                             group,
                             "type",
                             circle ? "circle" : "box");

      if (HW_IS_CIRCLE (child) || CLUTTER_IS_RECTANGLE (child))
        {
          gchar *color_string;
          ClutterColor color;

          if (circle)
            hw_circle_get_color (HW_CIRCLE (child), &color);
          else
            clutter_rectangle_get_color (CLUTTER_RECTANGLE (child), &color);

          color_string = clutter_color_to_string (&color);
          g_key_file_set_string (key_file,
                                 group,
                                 "color",
                                 color_string);
          g_free (color_string);
        }

      if (CLUTTER_IS_TEXTURE (child))
        {
          const gchar *file = g_object_get_data (G_OBJECT (child), "image");
          if (file)
            g_key_file_set_string (key_file,
                                   group,
                                   "image",
                                   file);
        }

      if (circle)
        {
          g_key_file_set_double (key_file,
                                 group,
                                 "radius",
                                 MIN (clutter_actor_get_width (child),
                                      clutter_actor_get_height (child)) / 2.f);
        }
      else
        {
          g_key_file_set_double (key_file,
                                 group,
                                 "width",
                                 clutter_actor_get_width (child));
          g_key_file_set_double (key_file,
                                 group,
                                 "height",
                                 clutter_actor_get_height (child));
        }

      g_key_file_set_double (key_file,
                             group,
                             "x",
                             roundf (clutter_actor_get_x (child)));
      g_key_file_set_double (key_file,
                             group,
                             "y",
                             roundf (clutter_actor_get_y (child)));

      clutter_container_child_get (CLUTTER_CONTAINER (box2d), child,
                                   "mode", &type,
                                   "manipulatable", &reactive,
                                   NULL);
      switch (type)
        {
        case CLUTTER_BOX2D_DYNAMIC:
          g_key_file_set_string (key_file,
                                 group,
                                 "physics",
                                 "dynamic");
          break;

        case CLUTTER_BOX2D_STATIC:
          g_key_file_set_string (key_file,
                                 group,
                                 "physics",
                                 "static");
          break;

        default:
          break;
        }

      if (reactive)
        g_key_file_set_boolean (key_file,
                                group,
                                "reactive",
                                reactive);

      g_free (group);
    }

  g_hash_table_unref (actor_count);
  g_list_free (children);

  for (j = joints; j; j = j->next)
    {
      const gchar *type;
      HwJoint *joint = j->data;

      switch (joint->type)
        {
        case HW_JOINT_REVOLUTE:
          type = "joint-revolute";
          break;

        case HW_JOINT_REVOLUTE2:
          type = "joint-revolute2";
          break;

        case HW_JOINT_DISTANCE:
          type = "joint-distance";
          g_key_file_set_double (key_file,
                                 joint->name,
                                 "length",
                                 joint->distance.length);
          g_key_file_set_double (key_file,
                                 joint->name,
                                 "frequency",
                                 joint->distance.frequency);
          g_key_file_set_double (key_file,
                                 joint->name,
                                 "damping",
                                 joint->distance.damping);
          break;

        case HW_JOINT_PRISMATIC:
          type = "joint-prismatic";
          g_key_file_set_double (key_file,
                                 joint->name,
                                 "min-length",
                                 joint->prismatic.min_length);
          g_key_file_set_double (key_file,
                                 joint->name,
                                 "max-length",
                                 joint->prismatic.max_length);
          hw_key_file_set_vertex (key_file,
                                  joint->name,
                                  "axis",
                                  &joint->prismatic.axis);
          break;
        }

      g_key_file_set_string (key_file,
                             joint->name,
                             "actor1",
                             joint->actor1);

      g_key_file_set_string (key_file,
                             joint->name,
                             "actor2",
                             joint->actor2);

      hw_key_file_set_vertex (key_file,
                              joint->name,
                              "anchor1",
                              &joint->anchor1);

      if (joint->type != HW_JOINT_REVOLUTE2)
        hw_key_file_set_vertex (key_file,
                                joint->name,
                                "anchor2",
                                &joint->anchor2);

      g_key_file_set_string (key_file,
                             joint->name,
                             "type",
                             type);
    }

  data = g_key_file_to_data (key_file, NULL, NULL);
  g_file_set_contents (path, data, -1, NULL);
  g_free (data);

  g_key_file_free (key_file);
}

static gboolean
hw_key_file_get_vertex (GKeyFile       *key_file,
                        const gchar    *group,
                        const gchar    *key,
                        ClutterVertex  *vertex,
                        GError        **error)
{
  gchar *vertex_string;
  gchar **vertex_split;

  if (!(vertex_string = g_key_file_get_string (key_file,
                                               group,
                                               key,
                                               error)))
    return FALSE;

  vertex_split = g_strsplit (vertex_string, ",", 3);
  g_free (vertex_string);

  if (!vertex_split[0] || !vertex_split[1] || !vertex_split[2])
    {
      g_strfreev (vertex_split);
      return FALSE;
    }

  vertex->x = atof (vertex_split[0]);
  vertex->y = atof (vertex_split[1]);
  vertex->z = atof (vertex_split[2]);

  g_strfreev (vertex_split);

  return TRUE;
}

ClutterActor *
hw_load (const gchar *path, GList **joints)
{
  gint i;
  gchar **groups;
  ClutterActor *box2d;

  GError *error = NULL;
  GKeyFile *key_file = g_key_file_new ();
  GHashTable *actors_by_name = g_hash_table_new (g_str_hash, g_str_equal);

  if (!g_key_file_load_from_file (key_file, path, G_KEY_FILE_NONE, &error))
    {
      if (error)
        {
          g_warning ("Error loading file: %s", error->message);
          g_error_free (error);
        }

      g_key_file_free (key_file);

      return NULL;
    }

  box2d = hw_scene_new ();

  groups = g_key_file_get_groups (key_file, NULL);

  for (i = 0; groups[i]; i++)
    {
      gboolean circle;
      gchar *type, *actor_name;
      gfloat value, x, y, density, friction, restitution;

      ClutterActor *actor = NULL;
      const gchar *group = groups[i];
      ClutterColor color = { 0xff, 0xff, 0xff, 0xff };

      if (g_str_equal (group, "Ground"))
        {
          gfloat width = g_key_file_get_double (key_file, group,
                                                "width", NULL);
          gfloat height = g_key_file_get_double (key_file, group,
                                                 "height", NULL);

          hw_scene_set_size (HW_SCENE (box2d), width, height);

          continue;
        }

      if (g_str_equal (group, "Launcher"))
        {
          gfloat x = g_key_file_get_double (key_file, group,
                                            "x", NULL);
          guint ammo = g_key_file_get_integer (key_file, group,
                                               "ammo", NULL);

          hw_utils_create_launcher (box2d, x, ammo);

          continue;
        }

      if (!g_key_file_has_key (key_file, group, "type", NULL))
        continue;

      type = g_key_file_get_string (key_file, group, "type", NULL);
      if (g_str_equal (type, "circle"))
        circle = TRUE;
      else if (g_str_equal (type, "box"))
        circle = FALSE;
      else
        {
          g_free (type);
          continue;
        }
      g_free (type);

      if (g_key_file_has_key (key_file, group, "image", NULL))
        {
          gchar *path = g_key_file_get_string (key_file, group, "image", NULL);
          actor = clutter_texture_new_from_file (path, &error);
          g_object_set_data (G_OBJECT (actor), "image", path);
          g_object_weak_ref (G_OBJECT (actor), (GWeakNotify)g_free, path);
        }
      else if (g_key_file_has_key (key_file, group, "color", NULL))
        {
          gchar *color_string =
            g_key_file_get_string (key_file, group, "color", NULL);
          clutter_color_from_string (&color, color_string);
          g_free (color_string);
        }

      if (!actor)
        {
          if (circle)
            actor = hw_circle_new_with_color (&color);
          else
            actor = clutter_rectangle_new_with_color (&color);
        }

      /* Set the actor name */
      actor_name = g_strdelimit (g_strdup (group), "0123456789", '\0');
      clutter_actor_set_name (actor, actor_name);
      g_free (actor_name);
      g_hash_table_insert (actors_by_name, (gpointer)group, actor);

      if (circle)
        {
          gfloat radius = 25;

          if (g_key_file_has_key (key_file, group, "radius", NULL))
            radius = g_key_file_get_double (key_file, group, "radius", NULL);

          clutter_actor_set_size (actor, radius * 2, radius * 2);
        }
      else
        {
          gfloat width, height;

          width = height = 50;
          if (g_key_file_has_key (key_file, group, "width", NULL))
            width = g_key_file_get_double (key_file, group, "width", NULL);
          if (g_key_file_has_key (key_file, group, "height", NULL))
            height = g_key_file_get_double (key_file, group, "height", NULL);

          clutter_actor_set_size (actor, width, height);
        }

      x = g_key_file_get_double (key_file, group, "x", NULL);
      y = g_key_file_get_double (key_file, group, "y", NULL);
      clutter_actor_set_position (actor, x, y);

      clutter_container_add_actor (CLUTTER_CONTAINER (box2d), actor);

      /* Setup the physical properties */
      if (circle)
        clutter_container_child_set (CLUTTER_CONTAINER (box2d), actor,
                                     "is-circle", TRUE,
                                     NULL);

      if (g_key_file_has_key (key_file, group, "physics", NULL))
        {
          gchar *mode =
            g_key_file_get_string (key_file, group, "physics", NULL);

          if (mode)
            {
              if (g_str_equal (mode, "static"))
                clutter_container_child_set (CLUTTER_CONTAINER (box2d), actor,
                                             "mode", CLUTTER_BOX2D_STATIC,
                                             NULL);
              else if (g_str_equal (mode, "dynamic"))
                clutter_container_child_set (CLUTTER_CONTAINER (box2d), actor,
                                             "mode", CLUTTER_BOX2D_DYNAMIC,
                                             NULL);

              g_free (mode);
            }
        }

      if (g_key_file_get_boolean (key_file, group, "reactive", NULL))
        clutter_container_child_set (CLUTTER_CONTAINER (box2d), actor,
                                     "manipulatable", TRUE,
                                     NULL);

      /* Read shape properties and assign reasonable defaults */
      if (circle)
        {
          density = 5.0f;
          friction = 0.6f;
          restitution = 0.5f;
        }
      else
        {
          density = 7.0f;
          friction = 0.4f;
          restitution = 0.f;
        }

      value = g_key_file_get_double (key_file, group, "density", &error);
      if (error)
        g_clear_error (&error);
      else
        density = value;
      value = g_key_file_get_double (key_file, group, "friction", &error);
      if (error)
        g_clear_error (&error);
      else
        friction = value;
      value = g_key_file_get_double (key_file, group, "restitution", &error);
      if (error)
        g_clear_error (&error);
      else
        restitution = value;

      clutter_container_child_set (CLUTTER_CONTAINER (box2d), actor,
                                   "density", density,
                                   "friction", friction,
                                   "restitution", restitution,
                                   NULL);
    }

  for (i = 0; groups[i]; i++)
    {
      gchar *type;
      ClutterActor *actor1, *actor2;

      HwJoint joint = { 0, };
      const gchar *group = groups[i];

      if (!g_key_file_has_key (key_file, group, "type", NULL))
        continue;

      type = g_key_file_get_string (key_file, group, "type", NULL);

      if (g_str_equal (type, "joint-revolute"))
        joint.type = HW_JOINT_REVOLUTE;
      else if (g_str_equal (type, "joint-revolute2"))
        joint.type = HW_JOINT_REVOLUTE2;
      else if (g_str_equal (type, "joint-distance"))
        joint.type = HW_JOINT_DISTANCE;
      else if (g_str_equal (type, "joint-prismatic"))
        joint.type = HW_JOINT_PRISMATIC;
      else
        {
          g_free (type);
          continue;
        }
      g_free (type);

      joint.actor1 = g_key_file_get_string (key_file, group, "actor1", NULL);
      if (!joint.actor1)
        continue;

      joint.actor2 = g_key_file_get_string (key_file, group, "actor2", NULL);
      if (!joint.actor2)
        {
          hw_joint_free (&joint, FALSE);
          continue;
        }

      hw_key_file_get_vertex (key_file,
                              group,
                              "anchor1",
                              &joint.anchor1,
                              NULL);

      if (joint.type != HW_JOINT_REVOLUTE)
        hw_key_file_get_vertex (key_file,
                                group,
                                "anchor2",
                                &joint.anchor2,
                                NULL);

      switch (joint.type)
        {
        case HW_JOINT_DISTANCE:
          joint.distance.length =
            g_key_file_get_double (key_file, group, "length", NULL);
          joint.distance.frequency =
            g_key_file_get_double (key_file, group, "frequency", NULL);
          joint.distance.damping =
            g_key_file_get_double (key_file, group, "damping", NULL);
          break;

        case HW_JOINT_PRISMATIC:
          joint.prismatic.min_length =
            g_key_file_get_double (key_file, group, "min-length", NULL);
          joint.prismatic.max_length =
            g_key_file_get_double (key_file, group, "max-length", NULL);
          hw_key_file_get_vertex (key_file,
                                  group,
                                  "axis",
                                  &joint.prismatic.axis,
                                  NULL);
          break;

        default:
          break;
        }

      joint.name = g_strdup (group);

      actor1 = g_hash_table_lookup (actors_by_name, joint.actor1);
      actor2 = g_hash_table_lookup (actors_by_name, joint.actor2);

      if (actor1 && actor2)
        {
          switch (joint.type)
            {
            case HW_JOINT_REVOLUTE:
              /*joint.joint =
                clutter_box2d_add_revolute_joint (CLUTTER_BOX2D (box2d),
                                                  actor1,
                                                  actor2,
                                                  &joint.anchor1,
                                                  &joint.anchor2);*/
              break;

            case HW_JOINT_REVOLUTE2:
              joint.joint =
                clutter_box2d_add_revolute_joint2 (CLUTTER_BOX2D (box2d),
                                                   actor1,
                                                   actor2,
                                                   &joint.anchor1);
              break;

            case HW_JOINT_DISTANCE:
              joint.joint =
                clutter_box2d_add_distance_joint (CLUTTER_BOX2D (box2d),
                                                  actor1,
                                                  actor2,
                                                  &joint.anchor1,
                                                  &joint.anchor2,
                                                  joint.distance.length,
                                                  joint.distance.frequency,
                                                  joint.distance.damping);
              break;

            case HW_JOINT_PRISMATIC:
              joint.joint =
                clutter_box2d_add_prismatic_joint (CLUTTER_BOX2D (box2d),
                                                   actor1,
                                                   actor2,
                                                   &joint.anchor1,
                                                   &joint.anchor2,
                                                   joint.prismatic.min_length,
                                                   joint.prismatic.max_length,
                                                   &joint.prismatic.axis);
              break;
            }
        }

      if (joints)
        *joints = g_list_append (*joints, g_slice_dup (HwJoint, &joint));
      else
        hw_joint_free (&joint, FALSE);
    }

  g_strfreev (groups);

  g_hash_table_unref (actors_by_name);

  g_key_file_free (key_file);

  return box2d;
}

void
hw_joint_free (HwJoint *joint, gboolean free)
{
  g_free (joint->name);
  g_free (joint->actor1);
  g_free (joint->actor2);

  if (free)
    g_slice_free (HwJoint, joint);
}

static void
hw_paint_offset_pre_cb (ClutterActor *actor,
                        guint         data)
{
  gint16 x = (gint16)(data >> 16);
  gint16 y = (gint16)(data & 0xFFFF);

  cogl_translate (-x, -y, 0);
}

static void
hw_paint_offset_post_cb (ClutterActor *actor,
                         guint         data)
{
  gint16 x = (gint16)(data >> 16);
  gint16 y = (gint16)(data & 0xFFFF);

  cogl_translate (x, y, 0);
}

void
hw_paint_offset (ClutterActor *actor, gint16 x, gint16 y)
{
  guint data;

  g_signal_handlers_disconnect_matched (actor,
                                        G_SIGNAL_MATCH_FUNC,
                                        0,
                                        0,
                                        NULL,
                                        hw_paint_offset_pre_cb,
                                        NULL);
  g_signal_handlers_disconnect_matched (actor,
                                        G_SIGNAL_MATCH_FUNC,
                                        0,
                                        0,
                                        NULL,
                                        hw_paint_offset_post_cb,
                                        NULL);

  if ((x == 0) && (y == 0))
    return;

  data = (((gint)x) << 16) | y;
  g_signal_connect (actor, "paint",
                    G_CALLBACK (hw_paint_offset_pre_cb),
                    GUINT_TO_POINTER (data));
  g_signal_connect_after (actor, "paint",
                          G_CALLBACK (hw_paint_offset_post_cb),
                          GUINT_TO_POINTER (data));
}

ClutterActor *
hw_utils_create_launcher (ClutterActor *scene, gfloat x, guint ammo)
{
  gfloat height;

  ClutterActor *launcher = hw_launcher_new ();

  clutter_actor_set_name (launcher, "Launcher");

  if (ammo)
    hw_launcher_set_ammo (HW_LAUNCHER (launcher), ammo);

  hw_scene_get_size (HW_SCENE (scene), NULL, &height);
  clutter_actor_set_position (launcher, x,
                              height - clutter_actor_get_height (launcher));
  clutter_container_add_actor (CLUTTER_CONTAINER (scene), launcher);

  return launcher;
}

#define MAX_CLOUDS 100

static void
hw_utils_explode_cb (ClutterAnimation *animation,
                     ClutterActor     *cloud)
{
  clutter_actor_destroy (cloud);
}

void
hw_utils_explode (ClutterActor *scene, gfloat x, gfloat y, gdouble intensity)
{
  gint i;

  MxTextureCache *cache = mx_texture_cache_get_default ();

  intensity = CLAMP (intensity, 0.0, 1.0);

  for (i = 0; i < MAX ((gint)(MAX_CLOUDS * intensity), 1); i++)
    {
      gdouble scale;
      gfloat new_x, new_y;
      ClutterAnimation *animation;

      ClutterActor *cloud = (ClutterActor *)
        mx_texture_cache_get_texture (cache, "smoke.png");

      new_x = g_random_int_range (-32, 32) * intensity;
      new_y = g_random_int_range (-32, 32) * intensity;
      clutter_actor_set_position (cloud, x + new_x, y + new_y);

      scale = g_random_double_range (1.0, 1.0 + 5 * intensity);
      clutter_actor_set_scale_with_gravity (
        cloud, scale, scale, CLUTTER_GRAVITY_CENTER);

      clutter_container_add_actor (CLUTTER_CONTAINER (scene), cloud);

      scale = g_random_double_range (0.5, 1.0 + 5 * intensity);

      if (new_x > 0)
        new_x = x + new_x + g_random_double_range (0, 50) * intensity;
      else
        new_x = x + new_x - g_random_double_range (0, 50) * intensity;
      if (new_y > 0)
        new_y = y + new_y + g_random_double_range (0, 50) * intensity;
      else
        new_y = y + new_y - g_random_double_range (0, 50) * intensity;

      animation = clutter_actor_animate (cloud, CLUTTER_EASE_OUT_QUAD, 500,
                                         "x", new_x,
                                         "y", new_y,
                                         "opacity", 0x00,
                                         "scale-x", scale,
                                         "scale-y", scale,
                                         NULL);
      g_signal_connect_after (animation, "completed",
                              G_CALLBACK (hw_utils_explode_cb), cloud);
    }
}

static ca_context *
hw_utils_init_sound ()
{
  gint error;
  ca_context *c;

  if ((error = ca_context_create (&c)) != 0)
    {
      g_warning ("Error creating sound context: %s", ca_strerror (error));
      return NULL;
    }

  ca_context_change_props (c,
                           CA_PROP_APPLICATION_NAME, "Happy Wombats",
                           CA_PROP_MEDIA_ROLE, "game",
                           CA_PROP_CANBERRA_CACHE_CONTROL, "volatile",
                           NULL);

  if ((error = ca_context_open (c)) != 0)
    {
      g_warning ("Error opening sound context: %s", ca_strerror (error));
      ca_context_destroy (c);
      return NULL;
    }

  return c;
}

static ca_context *
hw_utils_get_sound_context ()
{
  static gboolean tried_init = FALSE;
  static ca_context *c = NULL;

  if (!c)
    {
      if (!tried_init)
        {
          c = hw_utils_init_sound ();
          tried_init = TRUE;
          return c;
        }
      return NULL;
    }

  return c;
}

static inline gchar *
hw_utils_sound_get_db (gdouble volume)
{
  static gchar vol_string[6];
  snprintf (vol_string, 6, "%lf", (1.0 - volume) * -20);
  return vol_string;
}

void
hw_utils_play_sound (const gchar *filename,
                     gdouble      x,
                     gdouble      y,
                     gdouble      volume)
{
  gint error;
  gchar x_string[6], y_string[6];
  ca_context *c = hw_utils_get_sound_context ();

  if (!c)
    return;

  snprintf (x_string, 6, "%lf", x);
  snprintf (y_string, 6, "%lf", y);

  error = ca_context_play (c, 2,
                           CA_PROP_MEDIA_FILENAME, filename,
                           CA_PROP_CANBERRA_VOLUME,
                             hw_utils_sound_get_db (volume),
                           CA_PROP_EVENT_MOUSE_HPOS, x_string,
                           CA_PROP_EVENT_MOUSE_VPOS, y_string,
                           NULL);
  if (error != 0)
    g_warning ("Error playing music: %s", ca_strerror (error));
}

void
hw_utils_play_music (const gchar *filename,
                     gdouble      volume)
{
  gint error;
  ca_context *c = hw_utils_get_sound_context ();

  if (!c)
    return;

  ca_context_cancel (c, 1);
  error = ca_context_play (c, 1,
                           CA_PROP_MEDIA_FILENAME, filename,
                           CA_PROP_CANBERRA_VOLUME,
                             hw_utils_sound_get_db (volume),
                           CA_PROP_CANBERRA_CACHE_CONTROL, "never",
                           NULL);
  if (error != 0)
    g_warning ("Error playing music: %s", ca_strerror (error));
}

void
hw_utils_shutdown ()
{
  ca_context *c = hw_utils_get_sound_context ();
  if (c)
    ca_context_destroy (c);
}
